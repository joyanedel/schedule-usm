import React from 'react';
import Calendar from './components/Calendar.js';
import './App.css';

class App extends React.Component {
  constructor(props){
    super(props);
    this.input = React.createRef();
  }
  
  render () {
    const calendario = <Calendar />
    return (
      <div>
        <h2 className="centering"> Horario académico </h2>
        {calendario}
      </div>
    );
  }
}

export default App;
