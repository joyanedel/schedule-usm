import React from 'react';
import Window from './Window';
import './Calendar.css';
import Schedule from './Schedule';
import Ramos from './Ramos';

const blocks = ["1-2","3-4","5-6","7-8","9-10","11-12","13-14","15-16"];
const days = ["Lu", "Ma", "Mi", "Ju", "Vi", "Sa", "Do"];

class Calendar extends React.Component{
  constructor(props){
    super(props);
    this.input = React.createRef();
    this.state = {
      blocks,
      days,
      schedule: Schedule.Horario,
      colores: Ramos,
      siglas: Schedule.Siglas,
      bloques: Schedule.Bloques
    }
    this.nodes = {};
    this.window = <Window className="centered" ref={(node) => this.nodes["window"] = node}/>
  }

  getNode = (node,id) => {
    this.nodes[id] = React.createRef();
    this.nodes[id] = node;
  }

  onFocus = id => {
    const node = this.nodes[id];
    const block = id.slice(2);
    const day = id.slice(0,2);
    const clase = this.state.schedule[day][block][1];
    node.style.background = "linear-gradient(to bottom, rgba(255,165,31,0.65) 0%,rgba(255,165,31,0) 100%)";
    this.nodes["window"].node.children[0].innerHTML = "Horario: "+this.state.bloques[block];
    this.nodes["window"].node.children[2].innerHTML = "Asignatura: "+this.state.siglas[node.innerHTML][0];
    this.nodes["window"].node.children[4].innerHTML = "Profesor: "+this.state.siglas[node.innerHTML][1];
    this.nodes["window"].node.children[6].innerHTML = "Clase: "+clase;
  }

  onBlur = (id,color) => {
    const node = this.nodes[id];
    node.style.background = color;
    this.nodes["window"].node.children[0].innerHTML = "Horario: ";
    this.nodes["window"].node.children[2].innerHTML = "Asignatura: ";
    this.nodes["window"].node.children[4].innerHTML = "Profesor: ";
    this.nodes["window"].node.children[6].innerHTML = "Clase: ";
  }

  render() {
    return(
      <div className="shower">
      <div class="split left">
        {this.window}
      </div>
      <div class="split right">
      <table>
        <tbody>
          <tr style={{background: 'linear-gradient(to bottom, rgba(240,171,42,1) 0%,rgba(248,207,156,0) 100%)'}}>
            <th> Bloque </th>
            <th> Lunes </th>
            <th> Martes </th>
            <th> Miércoles </th>
            <th> Jueves </th>
            <th> Viernes </th>
            <th> Sábado </th>
            <th> Domingo </th>
          </tr>
        {this.state.blocks.map(block => {
          return(
            <tr key={block}>
              <td> {block} </td>
              {this.state.days.map(day => {
                try{
                  const ramo = this.state.schedule[day][block][0];
                  if(ramo === undefined){throw console.error();}
                  const color = this.state.colores[ramo];
                  return(<td
                    className="td-ramos"
                    id={day+block}
                    key={day}
                    style={{background: color}}
                    ref={(node) => this.getNode(node, day+block)}
                    onMouseOver={() => {this.onFocus(day+block)}}
                    onMouseOut={() => this.onBlur(day+block, color)}
                    >{ramo}</td>);
                }
                catch(err){
                  return(<td key={day}></td>);
                }
              })}
            </tr>
          );
        })}

        </tbody>
      </table>
      </div>
      </div>
    );
  }
}

export default Calendar;
