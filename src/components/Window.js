import React from 'react';
import './Window.css';

class Window extends React.Component{
  constructor(props){
    super(props);
    this.node = React.createRef();
  }
  render(){
    return(
      <div ref={(node) => {this.node = node}}>
        <span id="bloque">Horario: </span>
        <br></br>
        <span id="ramo">Asignatura: </span>
        <br></br>
        <span id="profesor">Profesor: </span>
        <br></br>
        <span id="clase">Clase: </span>
      </div>
    );
  }
}

export default Window;
